package com.lecuong.democrud.repository;

import com.lecuong.democrud.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

    @Query(value = "SELECT * FROM contact c WHERE c.name LIKE %?1%", nativeQuery = true)
    List<Contact> findByNameContaining(String term);
}
