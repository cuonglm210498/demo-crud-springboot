package com.lecuong.democrud.service.impl;

import com.lecuong.democrud.entity.User;
import com.lecuong.democrud.repository.UserRepository;
import com.lecuong.democrud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean insertUser(User user) {

        User user1 = userRepository.save(user);

        //kiem tra user da duoc luu vao database hay chua, co null ko
        if (user1.getId() > 0){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }
}
