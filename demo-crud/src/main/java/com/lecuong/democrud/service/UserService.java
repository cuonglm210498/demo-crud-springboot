package com.lecuong.democrud.service;

import com.lecuong.democrud.entity.User;

public interface UserService {

    boolean insertUser(User user);

    User findUserByEmail(String email);
}
