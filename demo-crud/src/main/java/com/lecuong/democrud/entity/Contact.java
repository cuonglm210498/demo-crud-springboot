package com.lecuong.democrud.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "contact")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotEmpty(message = "Tên không được để trống")
    @Column(name = "name", nullable = false)
    private String name;

    @Email(message = "Bạn phải nhập đúng định dạng email")
    @NotEmpty(message = "Email không được để trống")
    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;
}
