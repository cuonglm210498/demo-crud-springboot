package com.lecuong.democrud.controller;

import com.lecuong.democrud.entity.User;
import com.lecuong.democrud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @GetMapping
    public String registerPage(){
        return "register";
    }

    @PostMapping
    public String register(@RequestParam String email,
                           @RequestParam String password,
                           @RequestParam String retypepassword,
                           ModelMap modelMap){

        boolean checkUserName = validate(email);

        if (checkUserName){
            if (password.equals(retypepassword)){
                User user = new User();
                user.setEmail(email);
                user.setPassword(passwordEncoder.encode(password)); // ma hoa mat khau nguoi dung
                boolean checkInsertUser = userService.insertUser(user);

                if (checkInsertUser){
                    modelMap.addAttribute("checkLogin", "Đăng ký thành công");
                }else{
                    modelMap.addAttribute("checkLogin", "Đăng ký thất bại");
                }
            }else {
                modelMap.addAttribute("checkLogin", "Mật khẩu không trùng khớp");
            }
        }else{
            modelMap.addAttribute("checkLogin", "Email không đúng định dạng");
        }

        return "register";

    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$", Pattern.CASE_INSENSITIVE);

    public static boolean validate(String userName){
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(userName);
        return matcher.find();
    }
}
